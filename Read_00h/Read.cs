﻿using NAND_Prog;
using NAND_Prog.UILib;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Read_00h
{
    //Ця DLL експортує операцію Read для чіпа (версія command 00h -> address -> data) 
    //using  in HY27US08281A

   
    [Export(typeof(Operation)),
        ExportMetadata("Name", "Read_00h")]
    public class Read : AbstractRead  //Конкретна реалізація операції ReadPage
    {
        public Read()
        {
            name = "Read";
            based = typeof(Page);

            optBox.AddOption(new UseSpareArea());       // Можна(це опція) читати додатково додаткову область

            Instruction instruction;
            //--------------------------------------------------------------
            instruction = new WriteCommand();                              //Створюю інструкцію WriteCommand
            chainInstructions.Add(instruction);                            //Додаю її в ланцюжок інструкцій операції ReadPage

            //    instruction.numberOfcycles = 0x01;                           //По дефолту 1       
            (instruction as WriteCommand).command = 0x00;
            (instruction as WriteCommand).Implementation = GetCommandMG;
            //--------------------------------------------------------------
            //--------------------------------------------------------------

            instruction = new WriteAddress();
            chainInstructions.Add(instruction);

            instruction.numberOfcycles = (Chip.memOrg.colAdrCycles + Chip.memOrg.rowAdrCycles); // /*(5Cycle)*/ Адрес буде підставляти кожна сторінка свій
            (instruction as WriteAddress).Implementation = GetAddressMG;
            //--------------------------------------------------------------
            
            //--------------------------------------------------------------

            instruction = new ReadData();
            chainInstructions.Add(instruction);
            (instruction as ReadData).Implementation = GetDataMG;
            //--------------------------------------------------------------
        }

        private BufMG GetDataMG(ChipPart client)                        //Реалізація _implementation для  ReadData : Instruction
        {


            DataMG meneger = new DataMG();
            meneger.direction = Direction.In;    //дані на вхід

            meneger.SetDataPlace((client as IDatable).GetDataPlace());

            ////Вираховую початок сторінки в файлі Chip.chipprovider.mmf (з врахуванням column)
            //meneger.dataOffset = ((client as Page).address >> (Chip.memOrg.colAdrCycles * 8))  // ‭0x1FFFF0000‬  (‭0001 1111 1111 1111 1111 0000 0000 0000 0000‬) --> 0x1FFFF (‭0001 1111 1111 1111 1111‬)
            //                         *
            //                         (ulong)((client as Page).size + (client as Page).spareArea_size)                  // 2112 байт
            //                         +
            //                         (client as Page).column;                                           // додатково зміщуюсь на відповідний стовбець , якщо 0 -то стаю на початок сторінки



            ////Вираховую розмір даних для читання в файл Chip.chipprovider.mmf (з врахуванням column)
            ////Тут я повинен звернутись до optBox і подивитись чи користувач перед запуском оперції поставив галочку optBox.readSpareArea
            ////і на основі цієї інформації і client._size + client._spareArea_size - client.column вирахувати розмір даних


            //if (this.optBox.IsCheckedOption(typeof(UseSpareArea)))
            //    meneger.numberOfCycles = ((client as Page).size + (client as Page).spareArea_size) - (client as Page).column;               // 2112 байт
            //else
            //    meneger.numberOfCycles = (client as Page).size - (client as Page).column;                                                  //2048 bytes


            //Вираховую початок сторінки в файлі Chip.chipprovider.mmf (з врахуванням column)
            //meneger.dataOffset = ((client as Page).address >> (Chip.memOrg.colAdrCycles * 8))  // ‭0x1FFFF0000‬  (‭0001 1111 1111 1111 1111 0000 0000 0000 0000‬) --> 0x1FFFF (‭0001 1111 1111 1111 1111‬)
            //                         *
            //                         (ulong)((client as Page).Size())                  // 2112 байт
            //                         +
            //                         (client as Page).column;                                           // додатково зміщуюсь на відповідний стовбець , якщо 0 -то стаю на початок сторінки

            meneger.dataOffset = (ulong)(client as Page).OffSet()
                                 +
                                 (client as Page).column;                                           // додатково зміщуюсь на відповідний стовбець , якщо 0 -то стаю на початок сторінки

            //Вираховую розмір даних для читання в файл Chip.chipprovider.mmf (з врахуванням column)
            //Треба вияснити чи можна писати в SpareArea!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            int x;
            //Тут я повинен звернутись до optBox і подивитись чи користувач перед запуском оперції поставив галочку optBox.needSpareArea
            //і на основі цієї інформації і client._size + client._spareArea_size - client.column вирахувати розмір даних


            if (this.optBox.IsCheckedOption(typeof(UseSpareArea)))
                meneger.numberOfCycles = (int)((client as Page).FullSize() - (client as Page).column);               // 2112 байт
            else
                meneger.numberOfCycles = (int)((client as Page).FullSize() - (client as Page).spareArea_size - (client as Page).column);                                                  //2048 bytes


            return meneger;





        }


    }
}
